# Docker Build Environment for Nachos

[![Docker Automated build](https://img.shields.io/docker/automated/nwtgck/nachos-build.svg)](https://hub.docker.com/r/nwtgck/nachos-build/)

A Docker Build Enviroment for [Nachos](https://homes.cs.washington.edu/~tom/nachos/)


## Reference

* https://homes.cs.washington.edu/~tom/nachos/